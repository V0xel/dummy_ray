#include "pch.h"
#include<cstdio>
#include<cstdlib>
#include<limits>

#define internal static
// Build as Single Compilation Unit (Unity Build)
#include "images.cpp"
#include "gammaTable.h"

constexpr auto F32_MAX = std::numeric_limits<f32>::max();

struct Ray
{
	glm::f32vec3 origin;
	glm::f32vec3 direction;
};

struct Material
{
	glm::f32vec3 albedo;
};

// Data layout will change (for SIMD)!
// its Sphere and Plane are same in terms of data (vec4)
// the thing that changes in an interpretation of this data
struct Sphere
{
	glm::f32vec3 center;
	f32 radius;
	u32 materialIndex;
};

struct Plane
{
	glm::f32vec3 normal;
	f32 offset;
	u32 materialIndex;
};

struct World
{
	Sphere* spheres;
	size_t sphereCount;
	Plane* planes;
	size_t planeCount;
	Material* materials;
	size_t materialsCount;
};

glm::f32vec3 pointAtRay(Ray r, const f32 t)
{
	return r.origin + t * r.direction;
}
// Note: Changed everything to pass by val (might be better cause structs are small)
f32 hitSphere(Sphere sphere, Ray ray, f32 tolerance)
{
	// TODO: Figure out why there is subtle change in lightning 
	// TODO: when using geometric one
#if 0
	// geometric solution
	glm::vec3 l = sphere.center - ray.origin;
	f32 tca = glm::dot(l, ray.direction);
	if (tca < tolerance) return F32_MAX;
	f32 d2 = glm::dot(l, l) - tca * tca;
	f32 r2 = sphere.radius * sphere.radius;
	if (d2 > r2) return F32_MAX;
	f32 thc = sqrt(r2 - d2);
	f32 t0 = tca - thc;
	f32 t1 = tca + thc;
	if (t0 > 0.0f && t0 < t1)
	{
		return t0;
	}
	return t1;

#else
	// analytic solution
	glm::vec3 oc = ray.origin - sphere.center;
	f32 a = 1.0f; // assuming normalized ray direction
	f32 b = 2.0f * glm::dot(oc, ray.direction);
	f32 c = dot(oc, oc) - sphere.radius * sphere.radius;

	f32 deltaSqrt = glm::sqrt(b * b - 4.0f * a*c);
	if (deltaSqrt > tolerance)
	{
		f32 t0 = (-b - sqrt(deltaSqrt)) / (2.0f*a);
		f32 t1 = (-b + sqrt(deltaSqrt)) / (2.0f*a);
		// Return the closest one
		if (t0 > 0 && t0 < t1)
		{
			return t0;
		}
		return t1;
	}
	return F32_MAX;
#endif
}

// !!!TODO: Something is fucked with offset
f32 hitPlane(Plane plane, Ray ray, f32 tolerance)
{
	f32 denom = glm::dot(plane.normal, ray.direction);
	if ((denom < -tolerance) > (denom > tolerance))
	{
		glm::f32vec3 temp = plane.offset - ray.origin;
		f32 t = glm::dot(temp, plane.normal) / denom;
		return t;
	}
	else
		return F32_MAX;
}

// Note: It will be changed just prototype code now
glm::f32vec3 castRay(World* world, Ray r)
{
	f32 hitCheckOffset = F32_MAX;
	f32 tolerance = 0.0001f;
	bool hitFlag = false; // Dirty flag for now
	glm::f32vec3 lightPos(1.5f, 2.0f, 0.3f);
	glm::f32vec3 outColor;

	// Spheres intersections
	for (size_t sphereI = 0; sphereI < world->sphereCount; sphereI++)
	{
		Sphere sphere = world->spheres[sphereI];
		f32 t = hitSphere(sphere, r, tolerance);
		if (t > 0.0f && t < hitCheckOffset)
		{
			hitCheckOffset = t;
			// Basic NoL+phong+fake ambient - single directional light
			glm::f32vec3 point = pointAtRay(r, t);
			glm::f32vec3 normal = glm::normalize(point - sphere.center);
			glm::f32vec3 lightDir = glm::normalize(lightPos - point);
			glm::f32vec3 viewDir = glm::normalize(-1.0f*r.direction);
			glm::f32vec3 halfway = glm::normalize(viewDir + lightDir);

			f32 spec = 1.0f * glm::pow(glm::max(0.0f, glm::dot(normal, halfway)), 256.0f);
			f32 NoL =  glm::max(0.0f, 1.1f *glm::dot(lightDir, normal));
			outColor = glm::clamp((world->materials[sphere.materialIndex].albedo * NoL + 
				(glm::f32vec3{ 0.1f, 0.26f, 1.0f } * 0.025f)) * (1.0f - spec) + spec, 0.0f, 1.0f);
			hitFlag = true;
		}
	}

	// Planes intersections
	for (size_t planeI = 0; planeI < world->planeCount; planeI++)
	{
		Plane plane = world->planes[planeI];
		f32 t = hitPlane(plane, r, tolerance);
		if (t > 0.0f && t < hitCheckOffset)
		{
			hitCheckOffset = t;
			// Basic lighting for now
			glm::f32vec3 point = pointAtRay(r, t);
			glm::f32vec3 lightDir = glm::normalize(lightPos - point);
			f32 NoL = glm::max(0.1f, 1.4f *glm::dot(lightDir, plane.normal));
			outColor = glm::clamp((world->materials[plane.materialIndex].albedo * NoL) + 
				glm::f32vec3{ 0.1f, 0.26f, 1.0f } * 0.025f, 0.0f, 1.0f);
			hitFlag = true;
		}
	}

	if (!hitFlag)
	{
		glm::f32vec3 unitDirection = glm::normalize(r.direction);
		f32 t = 0.5f * (unitDirection.y + 1.0f);
		outColor = glm::mix({ 1.0f, 1.0f, 1.0f }, glm::f32vec3{ 0.1f, 0.26f, 1.0f }, t);;
	}
	
	return outColor;
}

int main()
{
	ImageU32 img = createImage(1920, 1080);

	// Camera creation - Gram�Schmidt process
	glm::vec3 camTarget(0.0f, 0.0f, -10.0f);
	glm::vec3 camPosition(0.0f, 1.0f, 4.2f);
	glm::vec3 camAxisZ(glm::normalize(camPosition - camTarget));
	glm::vec3 camAxisX(glm::normalize(glm::cross({ 0.0f, 1.0f, 0.0f }, camAxisZ )));
	glm::vec3 camAxisY(glm::normalize(glm::cross(camAxisZ, camAxisX)));

	// Image plane (film) variables
	f32 distFromCenter = 1.0f;
	f32 imgPlaneW = 1.0f;
	f32 imgPlaneH = 1.0f;
	// Aspect ratio handle
	if (img.height > img.width)
	{
		imgPlaneW = imgPlaneH * (f32)img.width / (f32)img.height;
	}
	else if (img.width > img.height)
	{
		imgPlaneH = imgPlaneW * (f32)img.height / (f32)img.width;
	}
	f32 imgPlaneHalfW = 0.5f * imgPlaneW;
	f32 imgPlaneHalfH = 0.5f * imgPlaneH;
	glm::vec3 imgPlaneCenter = camPosition - distFromCenter*camAxisZ;

	Ray ray = { camPosition };
	glm::vec3 color;
	u32 colorRGB;

	// World creation
	World world = {};
	world.materialsCount = 3;
	world.materials = new Material[world.materialsCount];
	world.materials[0].albedo = { 1.0f, 0.0f, 0.0f };
	world.materials[1].albedo = { 0.0f, 1.0f, 0.0f };
	world.materials[2].albedo = { 0.0f, 0.0f, 1.0f };

	// Dirty spheres creation, also for now everyhing is defined in imagePlane space
	world.sphereCount = 3;
	world.spheres = new Sphere[world.sphereCount];
	world.spheres[0].center = { 0.0f, 0.0f, -1.0f };
	world.spheres[0].radius = 0.5f;
	world.spheres[0].materialIndex = 0;

	world.spheres[1].center = { 0.6f, 0.2f, -1.5f };
	world.spheres[1].radius = 0.5f;
	world.spheres[1].materialIndex = 1;

	world.spheres[2].center = { -0.7f, -0.3f, -0.27f };
	world.spheres[2].radius = 0.5f;
	world.spheres[2].materialIndex = 2;

	// Plane creation
	world.planeCount = 1;
	world.planes = new Plane[world.planeCount];
	world.planes[0].normal = glm::f32vec3(0.0f, 1.0f, 0.0f);
	world.planes[0].offset = -1.f;
	world.planes[0].materialIndex = 0;


	// Main rendering loop
	u32* imageBuffer = img.pixels;
	for (s32 j = img.height - 1; j >= 0; j--)
	{
		const f32 v = -1.0f + 2.0f * f32(j) / f32(img.height);

		for (s32 i = 0; i < img.width; ++i)
		{
			const f32 u = -1.0f + 2.0f * f32(i) / f32(img.width);
			glm::vec3 imgPlanePoint = imgPlaneCenter + u * imgPlaneHalfW*camAxisX + v * imgPlaneHalfH*camAxisY;
			// lazy, for now, always normalize ray direction
			ray.direction = glm::normalize(imgPlanePoint - ray.origin); 

			color = (glm::vec3)(castRay(&world, ray));
			color.r = adjustGamma(color.r);
			color.g = adjustGamma(color.g);
			color.b = adjustGamma(color.b);

			*imageBuffer++ = packColorToRGB(color.r, color.g, color.b);
		}
	}

	saveAsPPM(img);
	return 0;
}