﻿struct ImageU32
{
	u32 width;
	u32 height;
	u32 totalPixels;
	u32* pixels;
};

ImageU32
createImage(u32 w, u32 h)
{
	ImageU32 img{};
	img.height = h;
	img.width = w;
	img.totalPixels = w * h;
	img.pixels = new u32[w*h];
	return img;
}

u32
packColorToRGB(u8 r, u8 g, u8 b)
{
	return 255 << 24 | b << 16 | g << 8 | r;
}

u32
packColorToRGBA(u8 r, u8 g, u8 b, u8 a)
{
	return a << 24 | b << 16 | g << 8 | r;
}

u8
extractU8FromU32(u32 col, u32 ch)
{
	return *((u8*)&col + ch);
}

void
saveAsPPM(ImageU32 img)
{
	FILE* file = nullptr;
	fopen_s(&file, "image.ppm", "wb+");
	fprintf(file, "P6\n%d %d\n255\n", img.width, img.height);
	u8 r, g, b;

	for (u32 j = 0; j < img.totalPixels; ++j)
	{
		// Extract cause no alpha needed
		r = extractU8FromU32(img.pixels[j], 0);
		g = extractU8FromU32(img.pixels[j], 1);
		b = extractU8FromU32(img.pixels[j], 2);
		fprintf(file, "%c%c%c", r, g, b);
	}
}